import { SET_ERROR_TEXT } from '../utils/ActionTypes';

export function setErrorText(errorText) {
    return {
        type: SET_ERROR_TEXT,
        payload: errorText
    };
}
