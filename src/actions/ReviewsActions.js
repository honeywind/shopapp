import { SET_REVIEWS_DATA, SET_REVIEW_RATE, SET_REVIEW_TEXT } from '../utils/ActionTypes';

export function setReviewsData(reviewsData) {
    return {
        type: SET_REVIEWS_DATA,
        payload: reviewsData
    };
}

export function setReviewRate(reviewRate) {
    return {
        type: SET_REVIEW_RATE,
        payload: reviewRate
    };
}


export function setReviewText(reviewText) {
    return {
        type: SET_REVIEW_TEXT,
        payload: reviewText
    };
}
