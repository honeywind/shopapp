import { SET_ITEMS_DATA, SET_ITEM_ID } from '../utils/ActionTypes';

export function setItemsData(itemsData) {
    return {
        type: SET_ITEMS_DATA,
        payload: itemsData
    };
}

export function setItemID(itemID) {
    return {
        type: SET_ITEM_ID,
        payload: itemID
    };
}
