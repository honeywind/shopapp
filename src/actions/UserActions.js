import { SET_USERNAME, SET_PASSWORD, SET_ACCESS_TOKEN, SET_USER_STATE } from '../utils/ActionTypes';

export function setUsername(username) {
    return {
        type: SET_USERNAME,
        payload: username
    };
}

export function setPassword(password) {
    return {
        type: SET_PASSWORD,
        payload: password
    };
}


export function setAccessToken(accessToken) {
    return {
        type: SET_ACCESS_TOKEN,
        payload: accessToken
    };
}

export function setUserState(isSignedIn) {
    return {
        type: "SET_USER_STATE",
        payload: isSignedIn
    };
}
