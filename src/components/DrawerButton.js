import React from 'react';
import { TouchableOpacity, Image, StyleSheet } from 'react-native';

const DrawerButton = ({ navigation }) => (
  <TouchableOpacity
    onPress={() =>navigation.openDrawer()}  
  >
    <Image
      source={require('../../images/menu-white.png')}
      style={styles.buttonImage}
  />
  </TouchableOpacity>
);

export default DrawerButton;

const styles = StyleSheet.create({
  buttonImage: {
    width: 26,
    height: 26,
    marginLeft:10
  }
});
