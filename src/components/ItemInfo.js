import React from 'react';
import { StyleSheet, Text, Image } from 'react-native';
import { Title, Card, CardItem } from 'native-base';
import { IMAGES_URL } from '../utils/Constants';
import { ITEM_SCREEN_BACKGROUND_COLOR, PRIMARY_TEXT_COLOR } from '../utils/Colors';

const ItemInfo = (props) => {

  const { itemID, itemName, itemDescription, imageID } = props;
  let imageSource = {uri: `${IMAGES_URL}${imageID}`};

    return (
          <Card>
            <CardItem style={styles.imageContainer}>
              <Image source={imageSource} style={styles.itemImage} />
            </CardItem>
            <CardItem style={styles.textContainer}>
              <Title style={styles.itemName}>{itemName}</Title>
              <Text style={styles.itemDescription}>{itemDescription}</Text>
            </CardItem>
          </Card>
    );
}

export default ItemInfo;

const styles = StyleSheet.create({
  imageContainer: {
     backgroundColor: ITEM_SCREEN_BACKGROUND_COLOR,
     justifyContent: 'center',
  },
  itemImage: {
    width: 270,
    height: 240,
  },
  textContainer: {
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  itemName: {
    textAlign: 'left',
    color: PRIMARY_TEXT_COLOR,
    fontWeight: 'bold',
  },
  itemDescription: {
    textAlign: 'left',
    marginTop: 5,

  },

});
