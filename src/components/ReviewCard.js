import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { Card, CardItem, Left, Right } from 'native-base';
import StarRating from 'react-native-star-rating';
import { PRIMARY_TEXT_COLOR } from '../utils/Colors';

const ReviewCard = (props) => {

const { reviewID, reviewAuthor, reviewDate, reviewText, reviewRate } = props;

    return (
      <Card>
        <CardItem>
          <StarRating
            maxStars={5}
            rating={reviewRate}
            emptyStar={require('../../images/star-outline.png')}
            fullStar={require('../../images/star-full.png')}
            disabled={true}
            starSize={20}
          />
        </CardItem>
        <CardItem>
          <Left>
            <Text note>{reviewAuthor}</Text>
          </Left>
          <Right>
            <Text note>{reviewDate}</Text>
          </Right>
        </CardItem>
        <CardItem>
          <Text style={styles.reviewText}>{reviewText}</Text>
        </CardItem>
      </Card>
    );
}

export default ReviewCard;

const styles = StyleSheet.create({
  reviewText: {
    color: PRIMARY_TEXT_COLOR,
    fontSize: 16
  }

});
