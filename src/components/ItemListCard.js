import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { Card, CardItem, Thumbnail, Left } from 'native-base';
import { IMAGES_URL } from '../utils/Constants';
import { PRIMARY_TEXT_COLOR } from '../utils/Colors';

export const ItemListCard = (props) => {

  const { itemID, itemName, itemDescription, imageID, onPressItem } = props;

  let imageSource = {uri: `${IMAGES_URL}${imageID}`};
console.log(imageSource);
    return (
      <Card>
        <CardItem  button onPress={() => {onPressItem()}} style={{flexDirection:'row'}}>
          <Left style={styles.leftContent}>
            <Thumbnail square source={imageSource} style={styles.itemImage}/>
          </Left>
          <CardItem style={styles.rightContent}>
            <Text style={styles.itemName}>{itemName}</Text>
            <Text note>{itemDescription}</Text>
          </CardItem>
        </CardItem>
      </Card>
    );
}


const styles = StyleSheet.create({
  leftContent: {
    flex: 1,
  },
  itemImage: {
    width: 100,
    height: 90,
  },
  rightContent: {
    flex: 2,
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginLeft: 10
  },
  itemName: {
    fontWeight: 'bold',
    color: PRIMARY_TEXT_COLOR
  },

});
