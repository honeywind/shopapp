import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { ERROR_TEXT_COLOR } from '../utils/Colors';

const ErrorComponent = (props) => {

  const { errorText } = props;

    return (
      <View>
        <Text style={styles.errorText}>{errorText}</Text>
      </View>
    );
}

export default ErrorComponent;

const styles = StyleSheet.create({
  errorText: {
    color: ERROR_TEXT_COLOR,
    fontSize: 12,
    marginTop: 10
  }
});
