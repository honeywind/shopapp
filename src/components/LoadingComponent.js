import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { LOADING_TEXT } from '../utils/Constants';
import { PRIMARY_TEXT_COLOR } from '../utils/Colors';

const LoadingComponent = (props) => {

  const { errorText } = props;

    return (
      <View style={styles.container}>
        <Text style={styles.text}>{LOADING_TEXT}</Text>
      </View>
    );
}

export default LoadingComponent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    color: PRIMARY_TEXT_COLOR
  }
});
