import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container, Content, Header, Body } from 'native-base';
import { Image, StyleSheet, AsyncStorage, TouchableOpacity, Text } from 'react-native';
import { DrawerItems } from 'react-navigation';
import { setUsername, setPassword, setAccessToken, setUserState } from '../actions/UserActions';
import { CATALOG_SCREEN_ROUTE, WELCOME_DRAWER_TEXT, LOGIN_SCREEN_ROUTE,
         LOGIN_DRAWER_BUTTON_TEXT, LOGOUT_DRAWER_BUTTON_TEXT } from '../utils/Constants';
import { DRAWER_HEADER_BACKGROUND_COLOR, PRIMARY_TEXT_COLOR, DRAWER_HEADER_TEXT_COLOR
} from '../utils/Colors';

class CustomDrawerContent extends Component {


  logoutUser() {
    this.removeUserData();
    this.props.navigation.navigate(CATALOG_SCREEN_ROUTE);
  }

  removeUserData() {
    this.props.setUserState(false);
    this.props.setUsername(undefined);
    this.props.setPassword(undefined);
    this.props.setAccessToken(undefined);
  }

  render() {
    return (
      <Container>
        <Header style={styles.header}>
          <Body>
            <Image
              source={require('../../images/user-transparent.png')}
              style={styles.headerImage}
            />
            { this.props.user.isSignedIn &&
              <Text style={styles.headerText}>{WELCOME_DRAWER_TEXT + this.props.user.username}</Text>
            }
          </Body>
        </Header>
        <Content>
          <DrawerItems {...this.props} />
          {
            (this.props.user.isSignedIn) ?
              <TouchableOpacity
                onPress={this.logoutUser.bind(this)}
                style={styles.button}
              >
                <Image
                  source={require('../../images/logout.png')}
                  style={styles.icon}
                />
                <Text style={styles.buttonText}>{LOGOUT_DRAWER_BUTTON_TEXT}</Text>
              </TouchableOpacity>
            :
            <TouchableOpacity onPress={
              () => {
                this.props.navigation.navigate(LOGIN_SCREEN_ROUTE);
              }}
              style={styles.button}
            >
              <Image
                source={require('../../images/login.png')}
                style={styles.icon}
              />
              <Text style={styles.buttonText}>{LOGIN_DRAWER_BUTTON_TEXT}</Text>
            </TouchableOpacity>
          }
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state, props) {
    return {
        user: state.user,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setUsername: (username) => {
            dispatch(setUsername(username));
        },
        setPassword: (password) => {
            dispatch(setPassword(password));
        },
        setAccessToken: (accessToken) => {
            dispatch(setAccessToken(accessToken));
        },
        setUserState: (isSignedIn) => {
            dispatch(setUserState(isSignedIn));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomDrawerContent);

const styles = StyleSheet.create({
  headerImage: {
    height: 110,
    width: 110,
    borderRadius: 75,
  },
  headerText: {
    color: DRAWER_HEADER_TEXT_COLOR,
    fontWeight: 'bold',
    marginTop: 10,
    marginLeft: 20
  },
  header: {
    height: 160,
    backgroundColor: DRAWER_HEADER_BACKGROUND_COLOR,
    paddingLeft: 10
  },
  icon: {
    width: 25,
    height: 25,
    marginRight: 30
  },
  button: {
    flexDirection: 'row',
    marginLeft: 15,
    marginTop: 10
  },
  buttonText: {
    fontWeight: 'bold',
    color: PRIMARY_TEXT_COLOR
  }
});
