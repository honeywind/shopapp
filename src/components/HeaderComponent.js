import React from 'react';
import { StyleSheet } from 'react-native';
import { Header, Left, Body, Right, Title } from 'native-base';
import DrawerButton from './DrawerButton';
import { MAIN_TITLE_TEXT } from '../utils/Constants';
import { HEADER_BACKGROUND_COLOR } from '../utils/Colors';

const HeaderComponent = (props) => {

  return (
    <Header style={styles.header}>
      <Left style={{alignItems: 'flex-start'}}>
        <DrawerButton navigation={props.navigation}
          style={styles.button}
        />
      </Left>
      <Body>
        <Title style={styles.title}>
          {props.title || MAIN_TITLE_TEXT}
        </Title>
      </Body>
      <Right></Right>
    </Header>
  );
}

export default HeaderComponent;

const styles = StyleSheet.create({
  header: {
    backgroundColor: HEADER_BACKGROUND_COLOR,
  },
  title: {
    fontWeight: 'bold'
  }
});
