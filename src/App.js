
import React, { Component } from 'react';
import { YellowBox, AsyncStorage } from 'react-native';
import RootNavigator from './navigators/RootNavigator';
import store, {persistor} from "./utils/store";
import { Provider } from 'react-redux';
import logger from 'redux-logger';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/lib/integration/react';
import LoadingComponent from './components/LoadingComponent';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

export default class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <PersistGate
          loading={<LoadingComponent />}
          persistor={persistor}
          >
        <RootNavigator />
      </PersistGate>
      </Provider>
    );
  }
}
