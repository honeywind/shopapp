import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text } from 'react-native';
import { Container, Content, Textarea, Button } from 'native-base';
import StarRating from 'react-native-star-rating';
import { setReviewRate, setReviewText } from '../actions/ReviewsActions';
import { setReviewsData} from '../actions/ReviewsActions';
import { fetchReviews, postReview } from '../utils/BackendCommunication';
import { SUBMIT_BUTTON_TEXT } from '../utils/Constants';
import { INPUT_BACKGROUND_COLOR, BUTTON_TEXT_COLOR } from '../utils/Colors';

class ReviewForm extends Component {

  postReview() {
    postReview(this);
  }

  render() {
    return (
      <Container>
        <Content contentContainerStyle={styles.container}>
          <Textarea
            style={styles.textArea}
            value={this.props.reviews.reviewText}
            onChangeText={
              (text) => this.props.setReviewText(text)
            }
          />
          <StarRating
            maxStars={5}
            rating={this.props.reviews.reviewRate}
            emptyStar={require('../../images/star-outline.png')}
            fullStar={require('../../images/star-full.png')}
            disabled={false}
            starSize={30}
            selectedStar={
              (rating) => this.props.setReviewRate(rating)
            }
          />

          <Button
            info
            style={styles.submitButton}
            onPress={this.postReview.bind(this)}
          >
            <Text style={styles.buttonText}>{SUBMIT_BUTTON_TEXT}</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state, props) {
    return {
        items: state.items,
        user: state.user,
        reviews: state.reviews,
        error: state.error,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setReviewRate: (reviewRate) => {
            dispatch(setReviewRate(reviewRate));
        },
        setReviewText: (reviewText) => {
            dispatch(setReviewText(reviewText));
        },
        setReviewsData: (reviewsData) => {
            dispatch(setReviewsData(reviewsData));
        },
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(ReviewForm);

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    flex:1,
    alignItems: 'flex-start',
    paddingLeft: 30
  },
  textArea: {
    backgroundColor: INPUT_BACKGROUND_COLOR,
    height: 150,
    width: 300,
    fontSize: 16,
    marginBottom: 10
  },
  submitButton: {
    width: 300,
    justifyContent: 'center',
    marginTop: 20
  },
  buttonText: {
    color: BUTTON_TEXT_COLOR,
    fontWeight: 'bold',
    fontSize: 16,
  },
});
