import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, FlatList, Image } from 'react-native';
import { Container, Content } from 'native-base';
import { ItemListCard } from '../components/ItemListCard';
import DrawerButton from '../components/DrawerButton';
import HeaderComponent from '../components/HeaderComponent';
import {setItemsData, setItemID} from '../actions/ItemsActions';
import { fetchItems } from '../utils/BackendCommunication';
import { ITEM_SCREEN_ROUTE } from '../utils/Constants';
import { CATALOG_SCREEN_BACKGROUND_COLOR } from '../utils/Colors';

class CatalogScreen extends React.Component {

  static navigationOptions = {
    drawerIcon: (<Image
      source={require('../../images/home.png')}
      style={{ height: 24, width: 24 }} />),
  }

  componentWillMount() {
    fetchItems(this);
  }

  renderItem = ({item}) => (
     <ItemListCard
       itemID={item.id}
       itemName={item.title}
       itemDescription={item.text}
       imageID={item.img}
       onPressItem={() => {
         this.props.setItemID(item.id)
         this.props.navigation.navigate(ITEM_SCREEN_ROUTE, {
           itemID:item.id,
           itemName:item.title,
           itemDescription:item.text,
           imageID:item.img,
           navigation: this.props.navigation
         })
       }
       }
     />
   );

  render() {

    return (
      <Container style={styles.container}>
        <HeaderComponent navigation={this.props.navigation} />
        <Content>
          <FlatList
            data={this.props.items.itemsData}
            renderItem={this.renderItem}
            keyExtractor={item => item.id.toString()}
          />
        </Content>
      </Container>
    );
  }
}


function mapStateToProps(state, props) {
    return {
        items: state.items
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setItemsData: (itemsData) => {
          dispatch(setItemsData(itemsData));
        },
        setItemID: (itemID) => {
            dispatch(setItemID(itemID));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CatalogScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: CATALOG_SCREEN_BACKGROUND_COLOR,
  },

});
