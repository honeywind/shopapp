import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import { Container, Content } from 'native-base';
import ItemInfo from '../components/ItemInfo';
import ReviewsSection from './ReviewsSection';
import HeaderComponent from '../components/HeaderComponent';
import { REVIEWS_TITLE } from '../utils/Constants';
import { PRIMARY_TEXT_COLOR } from '../utils/Colors';

class ItemScreen extends Component {
  render() {
    const { navigation } = this.props;
    const { itemID, itemName, itemDescription, imageID} = this.props.navigation.state.params;
    return (
      <Container>
        <HeaderComponent navigation={this.props.navigation} />
        <Content>
          <ItemInfo
            itemID={itemID}
            itemName={itemName}
            itemDescription={itemDescription}
            imageID={imageID}
          />
          <Text style={styles.title}>{REVIEWS_TITLE}</Text>
          <ReviewsSection
            itemID={itemID}
            navigation={this.props.navigation}
          />
        </Content>
      </Container>
    );
  }
}

export default ItemScreen;

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    marginTop: 20,
    marginBottom: 10,
    marginLeft: 10,
    fontWeight: 'bold',
    color: PRIMARY_TEXT_COLOR
  },
});
