import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { Container, Content, Button } from 'native-base';
import HeaderComponent from '../components/HeaderComponent';
import ErrorComponent from '../components/ErrorComponent';
import { setUsername, setAccessToken, setUserState, setPassword } from '../actions/UserActions';
import { setErrorText} from '../actions/ErrorActions';
import { authorize } from '../utils/BackendCommunication';
import { LOGIN_URL, LOGIN_TITLE_TEXT, LOGIN_BUTTON_TEXT, NEW_USER, REGISTER_BUTTON_TEXT,
  USERNAME_PLACEHOLDER, PASSWORD_PLACEHOLDER, REGISTER_SCREEN_ROUTE } from '../utils/Constants';
import { PRIMARY_TEXT_COLOR, AUTHORIZE_SCREEN_BACKGROUND_COLOR, SEPARATOR_COLOR,
        INPUT_BACKGROUND_COLOR, BUTTON_TEXT_COLOR } from '../utils/Colors';

class LoginScreen extends Component {

  logIn() {
    authorize(this, LOGIN_URL);
  }

  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <HeaderComponent navigation={this.props.navigation}
          title={LOGIN_TITLE_TEXT}/>
        <Content contentContainerStyle={styles.container}>
          <TextInput
            placeholder={USERNAME_PLACEHOLDER}
            underlineColorAndroid= 'transparent'
            style={styles.input}
            value={this.props.user.username}
            onChangeText={(text) => this.props.setUsername(text)}
          />
          <TextInput
            placeholder={PASSWORD_PLACEHOLDER}
            underlineColorAndroid= 'transparent'
            style={styles.input}
            value={this.props.user.password}
            onChangeText={(text) => this.props.setPassword(text)}
            secureTextEntry={true}
          />
          <Button
            info
            style={styles.submitButton}
            onPress={this.logIn.bind(this)}
          >
            <Text style={styles.buttonText}>{LOGIN_BUTTON_TEXT}</Text>
          </Button>
          <ErrorComponent errorText={this.props.error.errorText} />
          <View style={styles.separator}/>
          <View style={styles.registerView}>
            <Text style={styles.newUserText}>{NEW_USER}</Text>
            <Button
              primary
              style={styles.registerButton}
              onPress={() => {
                this.props.setUsername(undefined);
                this.props.setPassword(undefined);
                this.props.setErrorText(undefined);
                this.props.navigation.navigate(REGISTER_SCREEN_ROUTE)
              }}
            >
              <Text style={styles.buttonText}>{REGISTER_BUTTON_TEXT}</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state, props) {
    return {
        user: state.user,
        error: state.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setUsername: (username) => {
            dispatch(setUsername(username));
        },
        setPassword: (password) => {
            dispatch(setPassword(password));
        },
        setAccessToken: (accessToken) => {
            dispatch(setAccessToken(accessToken));
        },
        setUserState: (isSignedIn) => {
            dispatch(setUserState(isSignedIn));
        },
        setErrorText: (errorText) => {
            dispatch(setErrorText(errorText));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent: 'center',
    backgroundColor: AUTHORIZE_SCREEN_BACKGROUND_COLOR,
    alignItems: 'center',
    flexDirection: 'column',
    paddingTop: 80
  },
  input: {
    backgroundColor: INPUT_BACKGROUND_COLOR,
    width: 300,
    height: 45,
    marginBottom: 5,
  },
  submitButton: {
    width: 300,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 10
  },
  buttonText: {
    color: BUTTON_TEXT_COLOR,
    fontWeight: 'bold',
    fontSize: 16,
  },
  registerView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  newUserText: {
    fontWeight: 'bold',
    fontSize: 18,
    color: PRIMARY_TEXT_COLOR,
    marginRight: 30
  },
  registerButton: {
    padding: 2,
    width: 120,
    height: 40,
    justifyContent: 'center'
  },
  separator: {
    marginTop: 70,
    marginBottom: 30,
    marginLeft: 20,
    marginRight: 20,
    borderColor: SEPARATOR_COLOR,
    borderWidth: StyleSheet.hairlineWidth,
    alignSelf: 'stretch'
  }
});
