import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, FlatList, Text } from 'react-native';
import { Card, CardItem, Button } from 'native-base';
import ReviewCard from '../components/ReviewCard';
import { setReviewsData} from '../actions/ReviewsActions';
import { fetchReviews } from '../utils/BackendCommunication';
import { REVIEW_BUTTON_TEXT, REVIEW_FORM_SCREEN_ROUTE } from '../utils/Constants';
import { ITEM_SCREEN_BACKGROUND_COLOR, BUTTON_TEXT_COLOR } from '../utils/Colors';

class ReviewsSection extends React.Component {

  componentWillMount() {
    fetchReviews(this);
  }

  renderItem = ({item}) => (
     <ReviewCard
       reviewID={item.id}
       reviewAuthor={item.created_by.username}
       reviewDate={(item.created_at.split('T'))[0]}
       reviewText={item.text}
       reviewRate={item.rate}
     />
   );

  render() {
    const { itemID, navigation, accessToken } = this.props;
    return (
        <Card>
          <CardItem style={styles.topCardItem}>
            {
              this.props.user.isSignedIn &&
              <Button info
                onPress={() => {
                  navigation.navigate(REVIEW_FORM_SCREEN_ROUTE, {
                    navigation: this.props.navigation
                  })
                }}
                style={styles.button}
              >
                <Text style={styles.buttonText}>{REVIEW_BUTTON_TEXT}</Text>
              </Button>
            }
          </CardItem>
          <CardItem style={styles.bottomCardItem}>
            <FlatList
              data={this.props.reviews.reviewsData}
              renderItem={this.renderItem}
              keyExtractor={item => item.id.toString()}
              scrollEnabled={true}
              style={styles.flatList}
            />
          </CardItem>
        </Card>
    );
  }
}

function mapStateToProps(state, props) {
    return {
        items: state.items,
        user: state.user,
        reviews: state.reviews
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setReviewsData: (reviewsData) => {
            dispatch(setReviewsData(reviewsData));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReviewsSection);

const styles = StyleSheet.create({
  bottomCardItem: {
    backgroundColor: ITEM_SCREEN_BACKGROUND_COLOR,
    paddingTop:0
  },
  topCardItem: {
    backgroundColor: ITEM_SCREEN_BACKGROUND_COLOR,
  },
  buttonText: {
    color: BUTTON_TEXT_COLOR,
    fontWeight: 'bold',
    fontSize: 15,
  },
  button: {
    width: 150,
    padding: 5,
    justifyContent: 'center'
  },
});
