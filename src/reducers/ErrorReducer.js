import { SET_ERROR_TEXT } from '../utils/ActionTypes';

const ErrorReducer = (state = {
    errorText: '',
}, action) => {
    switch(action.type) {
        case SET_ERROR_TEXT:
            state = {
               ...state,
                errorText: action.payload
            };
            break;
    }
    return state;
};

export default ErrorReducer;
