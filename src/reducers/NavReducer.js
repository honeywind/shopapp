import { RootNavigator } from '../navigators/RootNavigator';
import { NavigationActions } from 'react-navigation';

const router = RootNavigator.router;
const initialAction = { type: NavigationActions.Init };
const initialState = RootNavigator.router.getStateForAction(initialAction);

export default (state = initialState, action) => {
   return router.getStateForAction(action, state)
}
