import { SET_REVIEWS_DATA, SET_REVIEW_RATE, SET_REVIEW_TEXT } from '../utils/ActionTypes';

const ReviewsReducer = (state = {
    reviewsData: [],
    reviewRate: 0,
    reviewText: ''
}, action) => {
    switch(action.type) {
        case SET_REVIEWS_DATA:
            state = {
               ...state,
                reviewsData: action.payload
            };
            break;
        case SET_REVIEW_RATE:
            state = {
               ...state,
                reviewRate: action.payload
            };
            break;
        case SET_REVIEW_TEXT:
            state = {
               ...state,
                reviewText: action.payload
            };
            break;
    }
    return state;
};

export default ReviewsReducer;
