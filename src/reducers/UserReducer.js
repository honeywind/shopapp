import { SET_USERNAME, SET_PASSWORD, SET_ACCESS_TOKEN, SET_USER_STATE } from '../utils/ActionTypes';

const UserReducer = (state = {
    username: undefined,
    password: undefined,
    accessToken: '',
    isSignedIn: false
}, action) => {
    switch(action.type) {
        case SET_USERNAME:
            state = {
               ...state,
                username: action.payload
            };
            break;
        case SET_PASSWORD:
            state = {
               ...state,
                password: action.payload
            };
            break;
        case SET_ACCESS_TOKEN:
            state = {
               ...state,
                accessToken: action.payload
            };
            break;
        case SET_USER_STATE:
            state = {
               ...state,
                isSignedIn: action.payload
            };
            break;
    }
    return state;
};

export default UserReducer;
