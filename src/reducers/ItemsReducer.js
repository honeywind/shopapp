import { SET_ITEMS_DATA, SET_ITEM_ID } from '../utils/ActionTypes';

const ItemsReducer = (state = {
    itemsData: [],
    itemID: 0
}, action) => {
    switch(action.type) {
        case SET_ITEMS_DATA:
            state = {
               ...state,
                itemsData: action.payload
            };
            break;
        case SET_ITEM_ID:
            state = {
               ...state,
                itemID: action.payload
            };
            break;
    }
    return state;
};

export default ItemsReducer;
