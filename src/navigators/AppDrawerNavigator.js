import React from 'react';
import { createDrawerNavigator } from 'react-navigation';
import CatalogScreen from '../containers/CatalogScreen';
import LoginScreen from '../containers/LoginScreen';
import ItemScreen from '../containers/ItemScreen';
import RegisterScreen from '../containers/RegisterScreen';
import CustomDrawerContent from '../components/CustomDrawerContent';

const AppDrawerNavigator = createDrawerNavigator({
  Home: {
    screen: CatalogScreen,
  },
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      drawerLabel: () => null
    }
  },
  Register: {
    screen: RegisterScreen,
    navigationOptions: {
      drawerLabel: () => null
    }
  },
  ViewItem: {
    screen: ItemScreen,
    navigationOptions: {
      drawerLabel: () => null
    }
  }
},
{
  contentComponent: (props) => (
    <CustomDrawerContent  {...props}/>
  ),
}
);

export default AppDrawerNavigator;
