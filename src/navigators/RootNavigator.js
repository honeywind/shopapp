import React, { Component } from 'react';
import { BackHandler } from "react-native";
import {createStackNavigator, addNavigationHelpers, NavigationActions} from 'react-navigation';
import { createReduxBoundAddListener, createReactNavigationReduxMiddleware
} from 'react-navigation-redux-helpers';
import { connect } from 'react-redux';
import CatalogScreen from '../containers/CatalogScreen';
import ItemScreen from '../containers/ItemScreen';
import LoginScreen from '../containers/LoginScreen';
import RegisterScreen from '../containers/RegisterScreen';
import ReviewForm from '../containers/ReviewForm';
import AppDrawerNavigator from './AppDrawerNavigator';
import HeaderComponent from '../components/HeaderComponent';
import { REVIEW_TITLE_TEXT } from '../utils/Constants';
import { HEADER_BACKGROUND_COLOR, TITLE_TEXT_COLOR } from '../utils/Colors';

export const RootNavigator = createStackNavigator({
  Home: {
    screen: CatalogScreen,
  },
  Drawer: {
    screen: AppDrawerNavigator,
    navigationOptions: ({navigation}) => ({
      header: null,
      headerMode: 'none'
    })
  },
  ViewItem: {
    screen: ItemScreen,
  },
  Login: {
    screen: LoginScreen
  },
  Register: {
    screen: RegisterScreen
  },
  AddReview: {
    screen: ReviewForm,
    navigationOptions: {
      title: REVIEW_TITLE_TEXT ,
      headerStyle: { backgroundColor: HEADER_BACKGROUND_COLOR },
      headerTitleStyle: { color: TITLE_TEXT_COLOR }
    }
  }

},
{
  initialRouteName: 'Drawer',
    headerMode: 'screen',
},
);


export const middleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.navigation,
);
const addListener = createReduxBoundAddListener("root");

class Nav extends Component {

  componentDidMount() {
      BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    componentWillUnmount() {
      BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress = () => {
      const { dispatch, navigation } = this.props;
      if (navigation.index === 0) {
        return false;
      }
      dispatch(NavigationActions.back());
      return true;
    };

  render() {
    return (
      <RootNavigator navigation={{
        dispatch: this.props.dispatch,
        state: this.props.navigation,
        addListener
      }} />
    )
  }
}

const mapStateToProps = state => ({
  navigation: state.navigation,
})

export default connect(mapStateToProps)(Nav);
