import { BASE_URL, PRODUCTS_URL, REVIEWS_URL, EMPTY_FIELD_ERROR } from './Constants';


export const fetchItems = async function(context) {
  try {
    let response = await fetch(
      `${BASE_URL}${PRODUCTS_URL}`
    );
    let responseJson = await response.json();
    context.props.setItemsData(responseJson);
  } catch (error) {
    console.error(error);
  }
};


export const fetchReviews = async function(context) {
  try {
    let response = await fetch(
      `${BASE_URL}${REVIEWS_URL}${context.props.items.itemID}`
    );
    let responseJson = await response.json();
    context.props.setReviewsData(responseJson);
  } catch (error) {
    console.error(error);
  }
}


export const postReview = async function(context) {
  try {
    let response = await fetch(`${BASE_URL}${REVIEWS_URL}${context.props.items.itemID}`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Token ${context.props.user.accessToken}`,
      },
      body: JSON.stringify({
        rate: context.props.reviews.reviewRate,
        text: context.props.reviews.reviewText,
      })
    });

    let responseText = await response.text();
    let responseJson = JSON.parse(responseText);

    if(responseJson.success === true) {
      fetchReviews(context);
      context.props.navigation.goBack();
    } else {
        let error = responseText;
        throw error;
    }
  } catch(error) {
    console.log('error: ' + error);
  }
};


export const authorize = async function(context, type) {
  if(context.props.user.username!== undefined && context.props.user.username!== undefined) {
    try {
      let response = await fetch(`${BASE_URL}${type}`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: context.props.user.username,
          password: context.props.user.password,
        })
      });

      let responseText = await response.text();
      let responseJson = JSON.parse(responseText);

      if(responseJson.success === true) {
        context.props.setErrorText(' ');
        let accessToken = responseJson.token;
        context.props.setAccessToken(accessToken);
        context.props.setUserState(true);
        context.props.navigation.navigate('Home');
      } else {
          let error = responseText;
          throw error;
      }
    } catch(error) {
      let errors = JSON.parse(error);
      context.props.setErrorText(errors.message);
    }
  }
  else {
    context.props.setErrorText(EMPTY_FIELD_ERROR);
  }
}
