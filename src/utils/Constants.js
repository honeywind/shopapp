export const BASE_URL = 'http://smktesting.herokuapp.com/api/';
export const LOGIN_URL = 'login/';
export const REGISTER_URL = 'register/';
export const PRODUCTS_URL = 'products/';
export const REVIEWS_URL = 'reviews/';
export const IMAGES_URL = 'http://smktesting.herokuapp.com/static/';
export const LOGIN_SCREEN_ROUTE = 'Login';
export const REGISTER_SCREEN_ROUTE = 'Register';
export const CATALOG_SCREEN_ROUTE = 'Home';
export const REVIEW_FORM_SCREEN_ROUTE = 'AddReview';
export const ITEM_SCREEN_ROUTE = 'ViewItem';
export const DRAWER_SCREEN_ROUTE = 'Drawer';

export const MAIN_TITLE_TEXT = 'Shop App';
export const LOGIN_TITLE_TEXT = 'Sign in';
export const REVIEW_TITLE_TEXT = 'New Review';
export const REGISTER_TITLE_TEXT = 'Sign up';

export const REVIEW_BUTTON_TEXT = 'SUBMIT A REVIEW';
export const SUBMIT_BUTTON_TEXT = 'SUBMIT';
export const LOGIN_BUTTON_TEXT = 'SIGN IN';
export const REGISTER_BUTTON_TEXT = 'REGISTER';
export const LOGIN_DRAWER_BUTTON_TEXT = 'Sign In';
export const LOGOUT_DRAWER_BUTTON_TEXT = 'Sign Out';

export const NEW_USER = 'New user?';
export const ALREADY_REGISTERED = 'Already registered?';
export const WELCOME_DRAWER_TEXT = 'Hello, ';
export const REVIEWS_TITLE = 'Ratings & Reviews';
export const EMPTY_FIELD_ERROR = 'Input data, please';
export const LOADING_TEXT = 'Loading...';

export const USERNAME_PLACEHOLDER = 'Username';
export const PASSWORD_PLACEHOLDER = 'Password';
