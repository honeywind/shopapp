import {createStore, applyMiddleware } from "redux";
import {createLogger} from "redux-logger";
import { AsyncStorage } from 'react-native';
import { persistCombineReducers, persistStore } from 'redux-persist'
import { middleware } from '../navigators/RootNavigator';
import navigation from "../reducers/NavReducer";
import user from "../reducers/UserReducer";
import items from "../reducers/ItemsReducer";
import reviews from "../reducers/ReviewsReducer"
import error from "../reducers/ErrorReducer";

const config = {
  key: 'primary',
  storage: AsyncStorage,
  whitelist: ['user'],
 }

 const reducer = persistCombineReducers(
   config,
   {
   navigation,
   user,
   items,
   reviews,
   error
  }
);

export default store = createStore(
  reducer,
  applyMiddleware(createLogger(), middleware),
);

export const persistor = persistStore(
  store
);
